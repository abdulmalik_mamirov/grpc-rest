# gRPC - Rest test task
Employees and Allocations services , Allocation for allocating different tasks to Empl
1. I've implemented basic MongoRepository to save 
   persist data, and created commandlineRunner in Employee Service to save sample data on startup
2. localhost:8089//employeeGrpc/{employeeID}/allocation  // e.g empID = 1 test endpoint 
   demonstrating grpc request to allocation service
3. localhost:8089/allocationRest/{allocationID} // e.g allId = 1
   demonstrating http request to allocation service
4. Basic microservice, eureka service-discovery service
5. Interface-service contains proto files
6. Dockerized services but there were some issue on Eureka connection,(need some workaround)
   but if maven build and run apps standalone works fine
7. In all basic demonstration, but in real work it'll be different in packaging, more clear


