package demo.allocation.service;

import java.util.ArrayList;
import java.util.List;

import demo.interfaces.grpc.Allocation;

public class AllocationResourceProvider {

	public static List<Allocation> getAllocationfromAllocationSource() {
		return new ArrayList<Allocation>() {
			{
				add(Allocation.newBuilder().setAllocationID(1l).setEmployeeID(1l).setProjectID(1l).build());
				add(Allocation.newBuilder().setAllocationID(2l).setEmployeeID(2l).setProjectID(1l).build());
				add(Allocation.newBuilder().setAllocationID(3l).setEmployeeID(3l).setProjectID(1l).build());
				add(Allocation.newBuilder().setAllocationID(4l).setEmployeeID(1l).setProjectID(2l).build());
			
			}
		};
	}
}
