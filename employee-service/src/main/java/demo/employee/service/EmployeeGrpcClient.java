package demo.employee.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import com.google.protobuf.Descriptors.FieldDescriptor;

import demo.interfaces.grpc.Allocation;
import demo.interfaces.grpc.AllocationServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.client.inject.GrpcClient;

@Service
public class EmployeeGrpcClient {

	@GrpcClient("allocation-service")
	private AllocationServiceGrpc.AllocationServiceBlockingStub allocationServiceBlockingStub;

	public List<Map<FieldDescriptor, Object>> getAllocationByEmployeeIDViaSynchronousClient(long employeeID) {

		Iterator<Allocation> allocationReply = allocationServiceBlockingStub
				.getAllocationByEmployee(Allocation.newBuilder().setEmployeeID(employeeID).build());

		//Convert Iterator to stream Stream<Allocation>
		Iterable<Allocation> iterable = () -> allocationReply;
		Stream<Allocation> allocationStream = StreamSupport.stream(iterable.spliterator(), false);

		return allocationStream.map(allocation -> allocation.getAllFields()).collect(Collectors.toList());

	}

}
