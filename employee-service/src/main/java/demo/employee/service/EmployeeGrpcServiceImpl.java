package demo.employee.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import demo.employee.service.domain.EmployeeEntity;
import demo.employee.service.repository.EmployeeRepository;
import demo.interfaces.grpc.Employee;
import demo.interfaces.grpc.EmployeeServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

@GrpcService
public class EmployeeGrpcServiceImpl extends EmployeeServiceGrpc.EmployeeServiceImplBase {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public void getEmployee(Employee request, StreamObserver<Employee> responseObserver) {
		Optional<EmployeeEntity> employeeEntity = employeeRepository.findById(request.getEmployeeID());
		responseObserver.onNext( Employee.newBuilder()
				.setEmployeeID(employeeEntity.get().getEmployeeID())
				.setEmployeeFirstName(employeeEntity.get().getEmployeeFirstName())
				.setEmployeeLastName(employeeEntity.get().getEmployeeLastName()).build());
		responseObserver.onCompleted();
	}


	
}
