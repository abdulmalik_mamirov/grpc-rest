package demo.employee.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.protobuf.Descriptors.FieldDescriptor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
public class EmployeeRestController {

	private static final String ALLOCATION_URL="http://localhost:8082";

	private WebClient webClient;
	private EmployeeGrpcClient employeeGrpcClient;

	public EmployeeRestController(final WebClient.Builder webClient,
								  final EmployeeGrpcClient employeeGrpcClient) {
		this.webClient = webClient.baseUrl(ALLOCATION_URL).build();
		this.employeeGrpcClient = employeeGrpcClient;
	}

	@RequestMapping(path = "/employeeGrpc/{employeeID}/allocation")
	public List<Map<FieldDescriptor, Object>> getAllocationByEmployeeID(@PathVariable Long employeeID) {
		
		return employeeGrpcClient.getAllocationByEmployeeIDViaSynchronousClient(employeeID);
	}

	@RequestMapping(path = "/allocationRest/{allocationID}")
	public Map<FieldDescriptor, Object>  getEmployeeByAllocationID(@PathVariable Long allocationID) {

		return webClient.get()
				.uri("/allocationGrpc/".concat(String.valueOf(allocationID)))
				.retrieve()
				.bodyToMono(Map.class).block();
	}
	
}
