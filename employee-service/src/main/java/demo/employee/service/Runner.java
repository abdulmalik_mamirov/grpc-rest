package demo.employee.service;

import demo.employee.service.domain.EmployeeEntity;
import demo.employee.service.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class Runner implements CommandLineRunner {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void run(final String... args) throws Exception {
            employeeRepository.save(new EmployeeEntity(1l, "Abdulmalik", "Mamirov" ));
			employeeRepository.save(new EmployeeEntity(2l, "Abdulloh", "Mamirov" ));
			employeeRepository.save(new EmployeeEntity(3l, "Oyatulloh", "Mamirov" ));
    }
}
