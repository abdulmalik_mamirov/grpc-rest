package demo.employee.service.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import static demo.employee.service.util.Contstants.COLLECTION_EMPLOYEE;

@Document(collection = COLLECTION_EMPLOYEE)
public class EmployeeEntity {
    private long employeeID ;
    private String employeeFirstName ;
    private String employeeLastName ;

    public EmployeeEntity(final long employeeID, final String employeeFirstName, final String employeeLastName) {
        this.employeeID = employeeID;
        this.employeeFirstName = employeeFirstName;
        this.employeeLastName = employeeLastName;
    }

    public long getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(final long employeeID) {
        this.employeeID = employeeID;
    }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public void setEmployeeFirstName(final String employeeFirstName) {
        this.employeeFirstName = employeeFirstName;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public void setEmployeeLastName(final String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }
}
